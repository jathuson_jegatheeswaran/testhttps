function splitBatch(Data)
   
   -- strip batch info off Data
   -- where is the \r being recognized from? 
   local a = Data:split('\r') 
   trace(a)
   -- recognizing data as the first segment
   Data=a[1]
   trace(Data)
   -- use a for loop to iterate through and append the remaining segments
   for i=2,#a do
      Data=Data..'\r'..a[i]
      trace(Data)
   end
   trace(Data)
   
   -- split Data into messages
   local delimiter='MSH|^~\\&|'
   -- the split function is splitting by the delimiter
   local b = Data:split(delimiter)

   trace(b)
   
  
   
   -- add MSH segment info
   
   -- this for loop iterates through each message and adds the delimiter to the front of the message
   -- what is the point of [i - 1] why could it not just be i? 
   for i=1,#b do 
      b[i-1]=delimiter..b[i]
      trace(b)
   end
   
   
   b[#b]=nil -- delete dup msg (nil does not print anything)
   trace(b)

   
   
   -- SOME IDEAS THAT COULD BE USEFUL

   -- global variable to count messages
   -- It will use the number of indexes on the table to determine the count
   MsgCnt = #b
   trace('Messages Count '..MsgCnt)
   
   -- globals for batch segment info
   
   
   -- For FHS everytime there is a | in a[1] there will be a new row created in the table
   FHS=a[1]:split('|')
   -- For BHS everytime there is a | in a[2] there will be a new row created in the table 
   BHS=a[2]:split('|')
   
-- This represents the last line    
   FTS=a[#a]:split('|') 
   
-- This represents the line before the last line 
   BTS=a[#a-1]:split('|') 

 
   return b

end


-- this function is responsible for parsing the hl7 message
function processMsg(Msg)
   local msg, name = hl7.parse{vmd='BARHL7.vmd', data= Msg}  
   return msg
end

-- this function is responsible for converting the terminators to a standard format for use 
function convertTerminators(Data)
   Data = Data:gsub('\r\n','\r')
   return Data:gsub('\n','\r')
end
