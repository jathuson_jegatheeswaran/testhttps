local providers = {}
local provider = {}


function providers.getProviderInfo(ID)

   if ProviderId == '4321' then         
      provider.EntityIdentifierCode='PR'
      provider.EntityTypeQualifier='2'
      provider.SourceName='Blue Medical Insurance'
      provider.IdCodeQualifier='PI'
      provider.InfoSourcePrimaryId='545454545'
      return  provider     
   elseif ProviderId == '9876' then
      provider.EntityIdentifierCode='PR'
      provider.EntityTypeQualifier='2'
      provider.SourceName='Green Medical Insurance'
      provider.IdCodeQualifier='PI'
      provider.InfoSourcePrimaryId='646464646'
      return provider       
   end

end

return providers