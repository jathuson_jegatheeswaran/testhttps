-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
--require stringutil for any functions that are called in the main 
require 'stringutil'
require 'recognizemessage'


-- gives back the results for the a, b, c, and d variables
local function trace(a,b,c,d) return end

-- This is the main function. Only functions called in the main function will be activated in the anotations window. 

function main(Data)
 
   -- Process batch file
   
   
   -- convert non-conformant terminators to "\r"
   Data = convertTerminators(Data)
   
   -- split batch into array of messages based on MSH 
   local Msgs=splitBatch(Data)
   
   -- populated flag controls the logic that enters
   -- payer and provider information one time into the
   -- respective transaction header envelope
   local populated= false 
   local msgCounter = 1
   
   
   -- this is where we call the module that will be responsible for
   -- administering templates, mappings and vmd
   local x12Message = require 'x12_mapfactory'
   local Outx12Msg = x12Message.create('4010-270')
   -- this loop is going through each one of the messages  
   for i=1,#Msgs do
      local OutHL7, name = hl7.parse{vmd='BARHL7.vmd', data= Msgs[i]}
      -- On first pass create the x12 doc and populate
      -- transactional header with payer and provider information
	-- why does populated have to be defined earlier and called false? 
      if populated == false then 
         
        -- Outx12Msg=x12Message.populateSubscr(Outx12Msg,OutHL7,msgCounter)
         populated=true
      end
      Outx12Msg = x12Message.populatePP(Outx12Msg,OutHL7)
      Outx12Msg=x12Message.populateSubscr(Outx12Msg,OutHL7,msgCounter)
      
       msgCounter = msgCounter + 1
     
   end
  
   
   local Out = Outx12Msg:S()
   Out = Out:gsub("|","*")
        trace(Out)
   
end



