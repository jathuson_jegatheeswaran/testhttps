require 'node'


local function trace(a,b,c,d) return end

-- define a local table to hold all of the references to functions and variables

local x12_mapfactory = {}

-- this is the template used for 4010 messages before importing the appropriate data from other message types
local function template_4010_IE(x12Msg)
   
   --Interchange Envelope Group
   x12Msg.InterchangeEnvelope.ISA[1]='00'
   x12Msg.InterchangeEnvelope.ISA[2]='          '
   x12Msg.InterchangeEnvelope.ISA[3]='00'
   x12Msg.InterchangeEnvelope.ISA[4]='          '
   x12Msg.InterchangeEnvelope.ISA[5]='ZZ'
   x12Msg.InterchangeEnvelope.ISA[6]='345529167'
   x12Msg.InterchangeEnvelope.ISA[7]='ZZ'
   x12Msg.InterchangeEnvelope.ISA[8]='445483154'
   x12Msg.InterchangeEnvelope.ISA[9]=os.date('%y%m%d')
   Time=os.time()
   x12Msg.InterchangeEnvelope.ISA[10]=os.date('%H%M',Time)
   x12Msg.InterchangeEnvelope.ISA[11]='U'
   x12Msg.InterchangeEnvelope.ISA[12]='00200'
   x12Msg.InterchangeEnvelope.ISA[13]=math.random(999999999)
   x12Msg.InterchangeEnvelope.ISA[14]='1'
   x12Msg.InterchangeEnvelope.ISA[15]='P'
   
   -- IEA Segment -- Interchange Envelope
   x12Msg.InterchangeEnvelope.IEA[1]='1'
   -- ISA and IEA control number must be the same
   x12Msg.InterchangeEnvelope.IEA[2]=x12Msg.InterchangeEnvelope.ISA[13]
   
end

local function template_4010_FG(x12Msg)
   
   -- GS Segment -- Functional Envelope Group
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[1]='HS'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[2]='1234567'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[3]='9876543'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[4]=os.date('%y%m%d')
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[5]=os.date('%H%M',Time)
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[6]=math.random(99999)
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[7]='X'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[8]='004010'
   local GSControlNumer=x12Msg.InterchangeEnvelope.FunctionalGroup[1].GS[6]
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GE[1]='1'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].GE[2]=GSControlNumer
      
end

local function template_4010_TG_270(x12Msg)
   -- ST Segment -- Transaction Envelope Group
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].ST[1]='270'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].ST[2]=math.random(99999)
   local STControlNumber=x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].ST[2]
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].SE[2]=STControlNumber
   
   -- ST Segment -- Transaction Envelope Group
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].BHT[1]='0016'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].BHT[2]='13'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1].BHT[3]='1000456' 
   
end

local function template_4010_PAY_270(x12Msg,hl7Msg)
   
    -- HL Segment -- Payer Header Information 
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.HL[1]='1'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.HL[3]='20'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.HL[4]='1'
  
   
   -- Get payer information from payers module
   local P = require 'payers'
   -- the :s() converts the node to a string 
   local payerId = hl7Msg.IN1[3][1]:S()
   local payer = P.getPayerInfo(payerId) 
   
  
   -- NM1 Segment -- Payer Information
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.Loop2100A.NM1[1]=payer.EntityIdentifierCode
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.Loop2100A.NM1[2]=EntityTypeQualifier
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.Loop2100A.NM1[3]=payer.SourceName
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.Loop2100A.NM1[8]=payer.IdCodeQualifier
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.Loop2100A.NM1[9]=payer.InfoSourcePrimaryId
   
end

local function template_4010_PRV_270(x12Msg,hl7Msg)
   
   -- HL Segment -- Provider Information
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.HL[1]='2'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.HL[2]='1'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.HL[3]='21'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.HL[4]='1'
   
  
   -- NM1 Segment -- Provider Information
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.Loop2100B.NM1[1]='1P'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.Loop2100B.NM1[2]='1'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.Loop2100B.NM1[3]='WELBY'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.Loop2100B.NM1[4]='Marcus'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.Loop2100B.NM1[8]='SV'
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].Loop2000B.Loop2100B.NM1[9]='23452435'  
  
end

local function template_4010_SUBR_270(x12Msg,hl7Msg,c)
   
   -- HL Segment -- Subcriber Information
   
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.HL[1]=c+2
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.HL[2]='2'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.HL[3]='22'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.HL[4]='0'
  
   -- NM1 Segment -- Subscriber Information
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[1]='IL'
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[2]='1' 
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[3]=hl7Msg.PID[5][1][1]
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[4]=hl7Msg.PID[5][2]
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[5]=hl7Msg.PID[5][3]
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[8]='MRN'
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.NM1[9]=hl7Msg.PID[3][1]
  
    -- DMG Segment -- Subscriber Demographic Information
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.DMG[2]=hl7Msg.PID[7][1]
  x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.DMG[3]=hl7Msg.PID[8]
   
   -- EQ Segment -- Subscriber Benefit Plan Information
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.Loop2110C[1].EQ[1]='30'
   x12Msg.InterchangeEnvelope.FunctionalGroup[1].TransactionSet[1]
   ["Position 010"].Grp010ABCD[1].Loop2000A.GrpBCD[1].GrpCD[c+2].Loop2000C.Loop2100C.Loop2110C[1].EQ[1]='FAM'  
end

local function create_4010_270(x12Msg)
   template_4010_IE(x12Msg)
   template_4010_FG(x12Msg)
   template_4010_TG_270(x12Msg)
end

-- This is the function that parses the x12 message 
function x12_mapfactory.create(x12Msg)
   if x12MsgType == '4010-270' then
      local Outx12Msg =x12.message{vmd='270v3.vmd',name='x270'}
      --Populate x12 message
      create_4010_270(Outx12Msg) 
      return Outx12Msg
   end 
end

function x12_mapfactory.populatePP(x12Msg, hl7Msg)   
   template_4010_PAY_270(x12Msg,hl7Msg)
   template_4010_PRV_270(x12Msg,hl7Msg)
   return x12Msg 
end

function x12_mapfactory.populateSubscr(x12Msg, hl7Msg,c)     
   template_4010_SUBR_270(x12Msg,hl7Msg,c)
   return x12Msg 
end

return x12_mapfactory