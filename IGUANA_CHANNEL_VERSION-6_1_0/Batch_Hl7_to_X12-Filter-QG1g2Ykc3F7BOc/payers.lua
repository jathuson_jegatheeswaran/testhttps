local payers = {}
local payer = {}


function payers.getPayerInfo(PayerID)

   if PayerID == '1234' then         
      payer.EntityIdentifierCode='PR'
      payer.EntityTypeQualifier='2'
      payer.SourceName='Blue Medical Insurance'
      payer.IdCodeQualifier='PI'
      payer.InfoSourcePrimaryId='545454545'
      return payer      
   elseif PayerID == '1235' then
      payer.EntityIdentifierCode='PR'
      payer.EntityTypeQualifier='2'
      payer.SourceName='Green Medical Insurance'
      payer.IdCodeQualifier='PI'
      payer.InfoSourcePrimaryId='646464646'
      return payer      
   end

end

return payers